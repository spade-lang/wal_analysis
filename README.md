## WAL Analysis Plugin for Swim

Use [WAL](https://github.com/ics-jku/wal) to analyze your simulation wave files.

## Usage

Add the plugin to your `swim.toml`
```toml
[plugins.wal_analysis]
git = "https://gitlab.com/spade-lang/wal_analysis"
branch="main"
```

To run analysis passes, run `swim pluggin analysis`
