from pathlib import Path
import json
import inspect
import glob
import sys
import os
import argparse
import functools
import importlib
from multiprocessing import Pool
from wal.util import Colors
from wal.reader import read_wal_sexprs
from wal.ast_defs import Symbol
from wal.ast_defs import Operator as Op
from wal.spade import WalAnalysisPass


class PlainWalPassWrapper(WalAnalysisPass):
    '''A wrapper to execute plain WAl passes '''

    def __init__(self, pass_dirs, wavefile, walfile):
        super().__init__(pass_dirs, wavefile)
        self.name = walfile
        self.walfile = Path(walfile).stem

    def run(self):
        self.wal.eval_str(f'(do (eval-file {self.walfile}) (run-pass))')


def find_python_passes(libraries_list):
    '''Seach for Python wrapped WAL passes in all directories
    passed in libraries_list.'''
    print(f'[{Colors.GREEN}INFO{Colors.END}] Searching WAL passes in libraries ...')
    found_passes = []
    for lib in libraries_list:
        wal_dir = lib['path'].joinpath('wal')
        if wal_dir.exists():
            # find all the Python files in wal_dir
            passes = glob.glob('*.py', root_dir=wal_dir)
            # scan every found Python file for Classes with the __wal_pass__ attribute
            for wal_pass in passes:
                path = str(wal_dir.joinpath(wal_pass).resolve().parent)
                sys.path.append(path)
                # import the python file we discovered earlier
                m = importlib.import_module(Path(wal_pass).stem)
                # this goes through all the defined things inside the python file and
                # filter out everything which is not
                # a class containing the __wal_pass__ attribute
                def is_pass(c):
                    return inspect.isclass(c) and hasattr(c, '__wal_pass__')

                passes_in_module = [x[1] for x in inspect.getmembers(m, is_pass)]
                # we store the classes + the path, since we need to append to WALPATH
                found_passes.append({
                    'path': str(wal_dir.resolve()),
                    'passes': passes_in_module
                })

    return found_passes

def find_wal_passes(libraries_list):
    '''Search for plain WAL passes in all directories passed in libraries_list.
       Plain WAL files with a run-pass function are wrapped on the fly inside
       a Python wrapper object.'''

    found_passes = []

    for lib in libraries_list:
        wal_dir = Path(lib['path'].joinpath('wal')).resolve()
        if wal_dir.exists():
            # find all the WAL files in wal_dir
            passes = glob.glob('*.wal', root_dir=wal_dir)
            for wal_pass in passes:
                pass_file = wal_dir.joinpath(wal_pass)
                with open(pass_file) as pass_file:
                    def is_pass(expr):
                        return isinstance(expr, list) \
                            and len(expr) > 3 \
                            and (expr[0] == Op.DEFMACRO \
                                 or expr[0] == Symbol('defun')) \
                            and expr[1] == Symbol('run-pass')

                    parsed = read_wal_sexprs(pass_file.read())
                    if any(map(is_pass, parsed)):
                        found_passes.append({
                            'path': str(wal_dir.resolve()),
                            'walfile': wal_pass
                        })

    return found_passes


def run_passes(wavefile, plugin_dir, wal_passes):
    wavefile_relative = wavefile.relative_to(os.getcwd())
    '''Run the passes in wal_passes on the trace wavefile '''
    for wal_pass in wal_passes:
        # get the location of each pass to add it to the WALPATH
        pass_dirs = [wal_pass['path'], f'{plugin_dir}/wal']
        
        if 'walfile' in wal_pass:
            # this is a plain WAL pass
            walfile = wal_pass['walfile']
            print(f'[{Colors.GREEN}INFO{Colors.END}] Running {walfile}')
            PlainWalPassWrapper(pass_dirs, wavefile, walfile).run()
        else:
            for sub_pass in wal_pass['passes']:
                pass_instance = sub_pass(pass_dirs, wavefile)
                # inside each Python file there can be multiple pass classes
                print(f'[{Colors.green("INFO")}] Running '\
                      f'{pass_instance.name} on {wavefile_relative}')
                pass_instance.run()

        print("")
    print("")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--plugin-dir')
    parser.add_argument("--test-list-file")
    parser.add_argument('-j', '--jobs', default='1'
                        , help='How many passes to run in parallel')

    args = parser.parse_args()
    plugin_dir = args.plugin_dir

    with open(args.test_list_file) as f:
        test_list = [Path(path) for path in json.loads(f.read())]

    # libraries
    build_dir = Path(args.test_list_file).parent
    with open(build_dir.joinpath('libraries.json')) as f:
        libraries_list = [{
            'name': library[0],
            'path': Path(library[1]).parent
        } for library in json.loads(f.read())]

    # search for passes in the this library
    libraries_list.append({
        'name': 'local',
        'path': Path('.')
    })
    # search for passes in this plugin
    libraries_list.append({
        'name': 'basic passes',
        'path': Path(plugin_dir)
    })

    wal_python_passes = find_python_passes(libraries_list)
    wal_plain_passes = find_wal_passes(libraries_list)

    with Pool(int(args.jobs)) as p:
        all_passes = wal_python_passes + wal_plain_passes
        p.map(functools.partial(run_passes
                                , wal_passes=all_passes
                                , plugin_dir=plugin_dir), test_list)


if __name__ == "__main__":
    main()
