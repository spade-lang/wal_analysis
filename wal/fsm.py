import pydot
from base64 import encodebytes
from pathlib import Path
from wal.spade import WalAnalysisPass


class FsmAnalysis(WalAnalysisPass):
    
    def __init__(self, pass_dir, wavefile):
        super().__init__(pass_dir, wavefile)
        self.name = "FSM Analysis"
        self.wal.eval_str('(eval-file "fsm")')
        self.wal.register_operator('dump-dot'
                                   , lambda seval, args: self.dump_dot(seval, args))
        self.wal.register_operator('show-dot'
                                   , lambda seval, args: self.show_dot(seval, args))
        self.IMAGE_CODE = '\033]1337;File=name=name;inline=true;:{}\a'

    def dump_dot(self, seval, args):
        output = seval.eval(args[0]).replace('::', '_')
        dot_data = seval.eval(args[1])
        (graph,) = pydot.graph_from_dot_data(dot_data)
        output_file = Path(f'build/{self.testname}/img/{output}.pdf')
        self.wal.eval_str(f'(log/analysis "Writing fsm plot to {output_file}")')
        output_file.parent.mkdir(parents=True, exist_ok=True)
        graph.write_pdf(output_file)

    def show_dot(self, seval, args):
        # shell must support iterm2 image protocol
        dot_data = seval.eval(args[0])
        (graph,) = pydot.graph_from_dot_data(dot_data)
        image_data = encodebytes(graph.create_png()).decode()
        print(f'\033]1337;File=name=name;inline=true;:{image_data}\a')

    def run(self):
        self.fsms = self.wal.eval_str('(define fsms (fsm-search))')
        
        if self.fsms:
            self.wal.eval_str('''
            (for [fsm fsms]
                 (let ([data (track-fsm fsm)])
                      ;; (fsm-gif fsm)
                      ;; (show-dot (fsm->dot/str data))
                      (dump-dot fsm (fsm->dot/str data))))''')
